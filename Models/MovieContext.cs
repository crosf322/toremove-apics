﻿using Microsoft.EntityFrameworkCore;

namespace LastProject.Models
{
    public class MovieContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<User> Users { get; set; }

        //la configuration à la base de données Sqlite locale
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=movies.db");

        //ou la configuration à la base de données MySql
        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseMySql("server=localhost;port=3306;userid=root;password=;database=films;");

        //Les relations entre les entités
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>(entity =>
            {
                entity.HasOne(d => d.Gender).WithMany(p => p.Movies);
            });
        }
    }
}
