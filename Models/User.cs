﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LastProject.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class AuthenticateToken
    {
        public string Token { get; set; }
    }
}
