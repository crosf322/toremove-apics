﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastProject.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MovieContext())
            {
                if (context.Movies.Any())
                {
                    return;   // DB has been seeded !
                }

                context.Genders.AddRange(
                    new Gender { Libelle = "Action" },
                    new Gender { Libelle = "Crime" },
                    new Gender { Libelle = "Comédie" },
                    new Gender { Libelle = "Horreur" },
                    new Gender { Libelle = "Drame" },
                    new Gender { Libelle = "Animation" },
                    new Gender { Libelle = "Musical" },
                    new Gender { Libelle = "Biographie" },
                    new Gender { Libelle = "Aventure" },
                    new Gender { Libelle = "Science-fiction" });

                context.SaveChanges();

                context.Movies.AddRange(
                        new Movie
                        {
                            Title = "100 mètres",
                            DateSortie = DateTime.Parse("2016-01-01"),
                            GenderId = 5,
                            Note = 4
                        },
                        new Movie
                        {
                            Title = "I Am Mother",
                            DateSortie = DateTime.Parse("2019-01-01"),
                            GenderId = 10,
                            Note = 4
                        },
                        new Movie
                        {
                            Title = "Catfight",
                            DateSortie = DateTime.Parse("2016-01-01"),
                            GenderId = 3,
                            Note = 4
                        });

                    string password = "test";
                    byte[] salt = Encoding.ASCII.GetBytes("9E2B946C399587AC62B15CD6CC78C");
                    string pwdhashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                            password: password,
                            salt: salt,
                            prf: KeyDerivationPrf.HMACSHA256,
                            iterationCount: 10000,
                            numBytesRequested: 256 / 8));

                    context.Users.Add(
                        new User { Username = "test", FirstName = "Test", LastName = "Test", Password = pwdhashed }
                    );

                context.SaveChanges();
            }
        }
    }
}
