﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LastProject.Models
{
    [Table("Movie")]
    public class Movie
    {
        public int Id { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9&\-'!\s]*$")]
        [StringLength(80, ErrorMessage = "Le titre ne peut être plus de 80 caractères.")]
        [Required(ErrorMessage = "Le titre est obligatoire")]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateSortie { get; set; }

        [Range(0, 5, ErrorMessage = "La note doit être contenue entre 0 et 5.")]
        public double Note { get; set; }

        [Required(ErrorMessage = "Le genre est obligatoire")]
        public int GenderId { get; set; }
        public Gender Gender { get; set; }
    }
}
